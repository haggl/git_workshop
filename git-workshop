#!/bin/sh
# Porcelain for git workshop


# ---  V A R I A B L E S  --- #

SCRIPT_NAME=$(basename $0)



# ---  S U B R O U T I N E S  --- #

print_help_message_and_exit() {
    cat << EOM
usage: git workshop [ -h ]
       git workshop < -c | -r | -s >
       git workshop generate-commits ...
       git workshop generate-repository ...
       git workshop generate-submodules ...

Git workshop utilities. Generates repositories with branches and submodules.

options:
  -h        show this help message
  -c        set up an exercise for dealing with merge conflicts
  -r        set up an exercise for rebasing / merging
  -s        set up an exercise for working with git submodules

subcommands:
    generate-commits
            generate and commit some random changes
    generate-repository
            set up a test repository
    generate-submodules
            create submodule(s)

    Use -h option to get detailed help on subcommands.
EOM
    exit
}

setup_merge_conflicts() {
    path="conflict_test_repo"
    git-workshop-generate-repository -b2 -c5 $path
    cd $path
    file=`ls | head -n -5 | tail -n 1`
    git-workshop-generate-commits -n 1  -o . -f $file
    git-workshop-generate-commits -n 5  -o .
    git-workshop-generate-commits -n 1  -o . -f $file -b test_branch1
    git-workshop-generate-commits -n 1  -o . -f no    -b test_branch1
    git-workshop-generate-commits -n 4  -o .          -b test_branch1
    git-workshop-generate-commits -n 1  -o . -f $file -b test_branch2
    git-workshop-generate-commits -n 3  -o .          -b test_branch2
    file=`ls | tail -n +5 | head -n 1`
    git-workshop-generate-commits -n 1  -o . -f $file
    git-workshop-generate-commits -n 3  -o .
    #git-workshop-generate-commits -n 1  -o . -f $file -b test_branch1
    git checkout test_branch1
    git rm $file
    git commit -m"Delete $file"
    git checkout master
    git-workshop-generate-commits -n 3  -o .          -b test_branch1
    git-workshop-generate-commits -n 1  -o . -f no    -b test_branch2
    git-workshop-generate-commits -n 1  -o . -f $file -b test_branch2
    git-workshop-generate-commits -n 2  -o .          -b test_branch2
}

setup_merge_playground() {
    path="rebase_test_repo"
    git-workshop-generate-repository $path
    cd $path
    git-workshop-generate-commits -n 4 -o . -b feature/not_a_bug
    git-workshop-generate-commits -n 5 -o .
    git-workshop-generate-commits -n 1 -o . -b develop
    git-workshop-generate-commits -n 8 -o . -b feature/1337-stuff -p develop
    git-workshop-generate-commits -n 6 -o . -b feature/hot-topic  -p develop
    git-workshop-generate-commits -n 2 -o . -b bugfix/issue42
}

setup_submodule_playground() {
            path="submodule_test_repo"
            git-workshop-generate-repository -s 2 -b 1 -c 5 $path
            cd $path
            git-workshop-generate-commits -n 4 -o . -b test_branch2
            cd submodule1
            file=`ls | head -n -5 | tail -n 1`
            git-workshop-generate-commits -n 3 -o .
            cd ../submodule2
            file=`ls | head -n -5 | tail -n 1`
            echo "Some changes..." >> $file
}



# ---  M A I N   S C R I P T  --- #

if [ -z "$1" -o "$1" = "-h" ]; then
    print_help_message_and_exit
fi

SUBCOMMAND=$1
shift

case "$SUBCOMMAND" in
    generate-commits|generate-repository|generate-submodules)
        git-workshop-$SUBCOMMAND $@
        ;;

    -c)
        setup_merge_conflicts
        ;;

    -r)
        setup_merge_playground
        ;;

    -s)
        setup_submodule_playground
        ;;

    *)
        print_help_message_and_exit
esac
