#!/bin/sh
# This plumbing command sets up a repository with a given number of branches and commits.


# ---  S U B R O U T I N E S  --- #
print_help_message() {
    echo "usage: `basename $0` [-b <number>] [-c <number>] [-n <number>]"
    echo
    echo "This script creates git repositories with some branches and adds them to"
    echo "the current repository as a git submodule."
    echo
    echo "options:"
    echo -e "  -b\tcreate <number> branches in addition to the master branch (default: $number_of_branches)"
    echo -e "  -c\tcreate <number> commits per branch (default: $number_of_commits)"
    echo -e "  -n\tcreate <number> submodules (default: $number_of_submodules)"
}



# ---  D E F A U L T   S E T T I N G S  --- #
number_of_branches=0
number_of_commits=10
number_of_submodules=1



# ---  M A I N   S C R I P T  --- #
while getopts ":b:c:n:" opt; do
    case $opt in
        b)
            number_of_branches=$OPTARG
            ;;

        c)
            number_of_commits=$OPTARG
            ;;

        n)
            number_of_submodules=$OPTARG
            ;;

        ?|:)
            print_help_message
            exit
    esac
done

git rev-parse || {
    echo "You must execute this script in the working directory of a git repository."
    exit
}

for n in `seq $number_of_submodules`; do
    path="submodule$n"
    git-workshop-generate-repository -b $number_of_branches -c $number_of_commits $path
    git submodule add ./$path
    git commit -m"Add $path"
done
